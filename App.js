import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Button,
  Text,
} from 'native-base';

import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  componentDidUpdate() {

    const firebaseConfig = {
      apiKey: 'AIzaSyBs64N4NDPvIwkNiNjn0f4sQgkUm5I9xVc',
      authDomain: 'my-firebase-authenticate.firebaseapp.com',
      databaseURL: 'https://my-firebase-authenticate.firebaseio.com',
      projectId: 'my-firebase-authenticate',
      storageBucket: 'my-firebase-authenticate.appspot.com',
      messagingSenderId: '422907417043',
      appId: '1:422907417043:web:ad1e91b2f6ab41efbedf26',
    };

    firebase.initialiizeApp(firebaseConfig);

  }

  signUp = () => {
    
    firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
    .then(res => {
      console.log("response => :", res);
    })
    .catch( err=> {
      console.log("response => :", err);
      alert(err.message);
    })
  }

  login = () => {
    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
    .then(res => {
      console.log("res => :", res);
    })
    .catch(err => {
      console.log("err => :", err);
    })
  }

  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Form>
            <Item>
              <Input autoCapitalize={"none"}
                onChangeText={(email) => (this.state.email = email)}
                placeholder="Username"
              />
            </Item>
            <Item last>
              <Input secureTextEntry
                onChangeText={(password) => (this.state.password = password)}
                placeholder="Password"
              />
            </Item>
          </Form>
          <Button full rounded style={{marginTop: 15}} onPress={this.signUp}>
            <Text>SIGNUP</Text>
          </Button>
          <Button full rounded warning style={{marginTop: 10}} onPress={this.login}>
            <Text>LOGIN</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
export default App;
